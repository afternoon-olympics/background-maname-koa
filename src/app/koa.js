const Koa = require("koa");
const app = new Koa();
// 引入body中间键
const KoaBodyparser = require("koa-bodyparser");
app.use(KoaBodyparser());
app.use(async (ctx, next) => {
  ctx.set("Access-Control-Allow-Origin", "*");
  await next();
});
// 引入路由
const UseRoute = require("../router/index.js");
app.UseRoute = UseRoute;
app.UseRoute();
// 静态文件夹中间键
const static = require("koa-static");
app.use(static("uploads"));

// 引入错误中间件
const errorHandler = require("./error-handle");
app.on("error", errorHandler);

// 导出
exports.app = app;
