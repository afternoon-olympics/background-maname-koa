const mysql =require("mysql2")
console.log(require('dotenv').config().parsed,1);
const pool=mysql.createPool({
  host:require('dotenv').config().parsed.DataBase_HOST,
  port:'3306',
  database:require('dotenv').config().parsed.DataBase_NAME,
  password:require('dotenv').config().parsed.DataBase_PASSWORD,
  user:"root",
})
pool.getConnection(function(err, connection){
  connection.connect(async(err) => {
    if (err) {
      console.log("连接失败:", err);
    } else {
      console.log("数据库连接成功~");
     await pool.promise().execute(`CREATE TABLE IF NOT EXISTS users (id INT PRIMARY KEY AUTO_INCREMENT,username VARCHAR(20) NOT NULL,password VARCHAR(100) NOT NULL,avatar VARCHAR(100) NOT NULL);`,[]);
     await pool.promise().execute(`CREATE TABLE IF NOT EXISTS images (id INT PRIMARY KEY AUTO_INCREMENT,name VARCHAR(20) NOT NULL,imageUrl VARCHAR(20) NOT NULL)`,[]);
    }
  })
});
module.exports=pool.promise();