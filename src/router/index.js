const fs = require("fs");
const path = require("path");
const UseRoute = function() {
  // 不要写./router 这样写是代表调用时候的相对目录
  fs.readdir(__dirname, (err, files) => {
    files.map((item) => {
        // 记得把自己return出去
    if (item === 'index.js') return;
    // 可以拿到路径拼接
      const router = require(path.resolve(__dirname, item));
    //   this为什么是app呢 因为都是箭头函数 所以取上层作用域 最上层是app
      this.use(router.routes());
    //   别漏了这个哦
      this.use(router.allowedMethods());
    });
  });
};
module.exports=UseRoute
// exports.UseRoute = UseRoute;