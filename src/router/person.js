// 测试例子
// localhost:8005/images
// {"name":"122","imageUrl":"1213","id":1}
const Router = require("koa-router");
const { getList, postData, putData, deleteData, anthVertify } = require("../common/middleware.js");
const router = new Router({
  prefix: "/images",
});
router.get("/", anthVertify, async (ctx, next) => {
  console.log(23);
  let id = ctx.request.query.id;
  await getList(ctx, { table: "images", otherState: id ? `WHERE id=${id}` : "" });
});
router.post("/", anthVertify, async (ctx, next) => {
  await postData({ table: "images", data: ctx.request.body, ctx });
});
router.put("/", anthVertify, async (ctx, next) => {
  await putData("images", ctx.request.body, ctx);
});
router.delete("/:id", async (ctx, next) => {
  let id = ctx.params.id;
  await deleteData("images", id, ctx);
});
module.exports = router;
